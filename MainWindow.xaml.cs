﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Lab3
{
    //Links: https://www.codeproject.com/Articles/483055/XML-Serialization-and-Deserialization-Part-1

    //https://social.msdn.microsoft.com/Forums/vstudio/en-US/d512b5d3-a98d-4970-a26b-15d07fd41e4b/get-xml-node-value-using-xpath-c?forum=csharpgeneral
    //https://docs.microsoft.com/pl-pl/dotnet/api/system.xml.serialization.xmlserializer?view=netframework-4.8

    //https://www.w3schools.com/xml/xpath_syntax.asp
    //http://www.csharp-examples.net/xpath-top-xml-nodes/

    public class CompoClass
    {
        [XmlIgnore]
        public double Precision { get; set; }
        [XmlAttribute("Type")]
        public string Model { get; set; }

       public CompoClass()
       {
            Precision = 0.0;
            Model = "Basic";
       }

        public CompoClass(double pre, string mod)
        {
            Precision = pre;
            Model = mod;
        }
    }
    [XmlType("MyClass")]
    public class MyClass
    {
        public MyClass()
        {
            Number = 0;
            Name = "Default";
            Component = null;
        }

        public MyClass(int num, string nam, CompoClass compo)
        {
            Number = num;
            Name = nam;
            Component = compo;
        }
        [XmlAttribute("Value")]
        public int Number { get; set; }
        public string Name { get; set; }
        [XmlElement("Component")]
        public CompoClass Component { get; set; }
    }
    public partial class MainWindow : Window
    {
        private List<MyClass> someList = new List<MyClass>()
        {
            new MyClass(1, "SuperA", new CompoClass(1.1, "CompoA")),
            new MyClass(2, "SuperB", new CompoClass(2.2, "CompoB")),
            new MyClass(3, "SuperC", new CompoClass(3.3, "CompoC")),
            new MyClass(4, "SuperD", new CompoClass(4.4, "CompoD")),
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        public void AddToCollection(object sender, RoutedEventArgs e)
        {
            var txtName = textBoxName.Text;
            var txtModel = textBoxModel.Text;
            var numberVal = (int)sliderNumber.Value;
            var precisionVal = sliderPrecision.Value;
            if (string.IsNullOrEmpty(txtName) || string.IsNullOrEmpty(txtModel))
            {
                MessageBox.Show("Puste pola!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            someList.Add(new MyClass(numberVal, txtName, new CompoClass(precisionVal, txtModel) ));
        }

        public void SerializeCollection(object sender, RoutedEventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(someList.GetType(), new XmlRootAttribute("MyClasses"));
            using (TextWriter writer = new StreamWriter(@"D:\Xml.xml"))
            {
               serializer.Serialize(writer, someList);
            }

            //Printing result
            var strBuilder = new StringBuilder();
            var rootNode = XElement.Load(@"D:\Xml.xml");
            using (var stringWriter = new StringWriter(strBuilder))
            {
                using (var xmlTextWriter = new XmlTextWriter(stringWriter))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;
                    rootNode.WriteTo(xmlTextWriter);
                }
            }

            textBoxOutput.Text = strBuilder.ToString();
        }

        private void DeserializeToCollection(object sender, RoutedEventArgs e)
        {
            XmlSerializer deserializer = new XmlSerializer(someList.GetType(), new XmlRootAttribute("MyClasses"));
            try
            {
                using (TextReader reader = new StreamReader(@"D:\Xml.xml"))
                {
                    someList = (List<MyClass>)deserializer.Deserialize(reader);
                }
                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }

            //Printing new contents of the list
            var outTxt = "";
            foreach (var el in someList)
            {
                outTxt += $"{el.Name}, {el.Number}, {el.Component.Precision}, {el.Component.Model} \n";
            }

            textBoxOutput.Text = outTxt;
        }

        private void SearchByXPath(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(@"D:\Xml.xml"))
            {
                MessageBox.Show("Brak pliku, najpierw musisz zserializowac!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var rootNode = XElement.Load(@"D:\Xml.xml");
            var found = rootNode.XPathSelectElements("MyClass[@Value<3]");
            string text = "";
            foreach (var el in found)
            {
                text += $"{el.Value}\n";
            }
            textBoxOutput.Text = text;
        }

        private void SearchByLINQ(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(@"D:\Xml.xml"))
            {
                MessageBox.Show("Brak pliku, najpierw musisz zserializowac!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var rootNode = XElement.Load(@"D:\Xml.xml");
            var found = from item in rootNode.Descendants("MyClass")
                where (int) item.Attribute("Value") < 3
                select item;

            string text = "";
            foreach (var el in found)
            {
                text += $"{el}\n";
            }
            textBoxOutput.Text = text;
        }
    }
}